# Analytics Engine Javascript Plugin
_V0.1_
This plugin is exclusively designed to be used with Analytics Engine. Head to [Analytics Engine Repository](#) to acquire the engine.

---

## Installation
Install using bower (Currently not public)
```
bower install aewidgets --save
```

Then include the necessary dependencies in your HTML
```html
<link rel="stylesheet" type="text/css" href="bower_components/aewidgets/css/aew.css" media="all">
<script src="bower_components/aewidgets/min/aew-min.js"></script>
```

> The minified javascript (aew-min.js) includes all the libraries. You don't need to declare jQuery and Chart.js dependencies in the HTML file.

## API
_For Backend API documentation, please refer to AE-API-URL_

Instantiate the object. The credentials can be obtained after you create a project at _AE-PROJECT-CREATOR-URL_
```javascript
var ae = new AE({
    username: 'Your Username',
    password: 'Password',
    project_id: 'Project Id',
    source_key: 'Source Key',
    url: 'Analytics Engine URL' // No cross domain support yet..
})
```

### Recording Events
Events are saved as JSON formatted strings. For the current version, we only support `string` & `integer`. You can save `objects` & `arrays` but there will be no way of querying those. To record an event, for example on page load:
```javascript
var data = {
    key1: 'str',
    key2: { k:v } // Unable to query nested objects
    action_type: 'str'
}
window.onload = function() {
    ae.record(data).then(function(response) {
        // Do whatever things here with the response object.
        console.log(response)
    })
}
```

Apart from the custom data that is recorded, this plugin also includes by default these data:

- `_referrer`: The originating URL, or 'Direct',
- `_time_spent`: 'N/A', will be implemented in the next phase,
- `_user_agent`: App that the user uses to access the URL, usually browser versions.

Available Promise methods returned by `$.record(data)` [JQuery Ajax Documentation](http://api.jquery.com/jquery.ajax/):

- `then()`
- `done()`
- `fail()`
- `always()`

### Built-in Widgets
- **Counter**
    Counter Widget will display a small card with the total count of your query.
```javascript
var queryObject = {
    date_range: 'all',
    data: { key1: 'str' }
}
ae.widget.counter({
    container: '#counter_container',
    title: 'optional',
    data: queryObject
})
```

- **Charts**
    This widget will display graphical charts of your query. For further customization of charts, refer to [Chart.js Documentation](http://www.chartjs.org/docs/) with an `options` object passed to the `chartType` property.
```javascript
var queryObject = {
    date_range: { days: 7 }, // { months: 12, years: 10 }
    query_param: { key2: 'str' }
}
ae.widget.chart({
    container: '#chart_container',
    title: 'Optional',
    chartType: 'Line', // or 'Bar', 'Pie',
    chartOptions : options
    data: queryObject
})
```

- **Trending**
    This widget will display a list of items that is trending e.g Popular Posts, sorted by highest count. The `list` key controls the trend reference.
```javascript
ae.widget.trending({
    container: '#trending_container',
    title: 'Optional',
    unit: 'Views' // Optional. This is to determine the count type.
    groupBy: 'key1', // The trending object
    data: queryObject
})
```

- **Feeds**
    This widget will display recently recorded events filtered by your query. The special `sentence` key will let you construct your own sentence to be displayed in the feed's list. If you need the sentence to use the key of the response, simply append `$` to mark it.
```javascript
ae.widget.feeds({
    container: '#trending_container',
    title: 'Optional',
    sentence: ['My', 'name', 'is', '$key1'],
    data: queryObject
})
```

### Querying Events
There are two types of query this plugin and Analytics Engine currently support; `count` and `raw_data`. This method is useful if you want to do things that is not supported by the built in methods. To query events;
```javascript
ae.query('count', queryObject).then(function(response) {
    // Do whatever with response object e.g display a Radar Chart.
    console.log(response)
})
```











