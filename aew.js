// Requires JQuery & Chartist (https://gionkunz.github.io/chartist-js/)
var AE = function (auth, url) {
    if (!auth) {
        console.log('Not enough data provided');
    } else {
        var _b64 = {_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=_b64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=_b64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};
        var url = url;
        var authdata = _b64.encode(auth.username + ':' + auth.password);
        var globalAjaxSettings = function(){
            return {
                headers: {
                    Authorization: 'Basic ' + authdata,
                    'SOURCE-KEY': auth.source_key
                },
                processData: false,
                contentType: 'application/json;',
                method: 'POST',
                error: function(xhr) {
                    try {
                        var a = JSON.parse(xhr.responseText)
                        console.error(a.detail)
                    } catch(e) {
                        if (xhr.status === 404) {
                            console.error('Invalid AE URL ('+ url +')')
                        } else {
                            console.error('An error occured.')
                        }
                    }
                }
            }
        };

        var template = {
            render: function(content, classname) {
                return '<section class="aew-container"><section class="' + classname + '">' + content + '</section></section>';
            },
            canvas: function(content, classname) {
                return '<section class="aew-container"><canvas height="80" class="' + classname + '">' + content + '</canvas></section>';
            },
            trending: function(list) {
                return '<ul class="aew-trending">' + list + '</ul>';
            },
            empty: function(context) {
                context = (context = '') ? 'This column' : context;
                return '<div class="aew-empty"><p>' + context + ' has no data.</p></div>'
            }
        };

        var methods = {
            record: function(data) {
                // Built-In Analytics Data
                var ae_url = auth.url + '/api/event/' + auth.project_id + '/';
                // Default data
                if (!data) { data = {} };
                data._referrer = document.referrer || 'Direct';
                data._time_spent = 'N/A';
                data._user_agent = window.navigator.userAgent;

                var ajax = new globalAjaxSettings();
                ajax.url = ae_url,
                ajax.data = JSON.stringify(data);

                return $.ajax(ajax)
            },
            query: function(type, data, success) {
                var querytype = '';
                switch(type) {
                    case 'raw':
                        querytype = '/api/query/raw_data/';
                    break;
                    case 'count':
                        querytype = '/api/query/count/';
                    break;
                }
                var ajax = new globalAjaxSettings();
                ajax.url = auth.url + querytype + auth.project_id + '/';
                ajax.data = JSON.stringify(data)

                return $.ajax(ajax);
            },
            widget: {
                counter: function(data) {
                    var ajax = new globalAjaxSettings();
                    ajax.url = auth.url + '/api/query/count/' + auth.project_id + '/';
                    ajax.data = JSON.stringify(data.data)
                    ajax.success = function(res) {
                        var count = (res.count != undefined ) ? res.count : res.total;
                        var title = (data.title != undefined) ? '<h3>' + data.title + '</h3>': '';
                        var tpl = '<h1>' + count + '</h1>';
                        $(data.container).html(template.render(title + tpl, 'aew-counter'));
                    }
                    $.ajax(ajax);
                },
                chart: function(data) {
                    var ajax = new globalAjaxSettings();
                    ajax.url = auth.url + '/api/query/count/' + auth.project_id + '/';
                    ajax.data = JSON.stringify(data.data);
                    ajax.success = function(res) {
                         // DOM Structure for charts
                        var tpl = '';
                        if (data.title) {
                            tpl = '<h3 class="aew-container-title">' + data.title + '</h3>';
                        }
                        tpl += template.canvas('', data.container.substr(1));

                        // Draw the DOM structure
                        $(data.container).html(tpl);

                        // 2 - Structure the response
                        try {
                            var labels = [];
                            var series = [];
                            res.results.forEach(function(v) {
                                var w = (v.week) ? 'Week ' + v.week : null
                                var label_name = w || v.month || v.date;
                                labels.push(label_name);
                                series.push(v.count);
                            })

                            var d = {
                                labels : labels.reverse(),
                                datasets: [
                                    { data: series.reverse() }
                                ]
                            }

                            if (data.chartType == 'Pie') {
                                d = []
                                series.forEach(function(v, i) {
                                    d.push({ value: v, label: labels[i] })
                                })
                            }

                            var opt = {
                                bezierCurve: false,
                                maintainAspectRatio: true,
                                responsive: true,
                                scaleShowVerticalLines: false,
                                datasetFill : false,
                            }

                            if (!data.chartType) {
                                data.chartType = 'Bar';
                            }

                            var ctx = $('.' + data.container.substr(1)).get(0).getContext('2d')
                            var chart = new Chart(ctx)[data.chartType](d, opt);

                        } catch(e) {
                            console.error(e)
                        }
                    };

                    $.ajax(ajax);
                },
                trending: function(data) {
                    var ajax = new globalAjaxSettings();
                    ajax.url = auth.url + '/api/query/raw_data/' + auth.project_id + '/';
                    ajax.data = JSON.stringify(data.data);
                    ajax.success = function(res) {
                        var events = processApiEvents(res);
                        var title = (data.title != undefined) ? data.title : '';
                        var tpl = '<h3>' + title + '</h3>';
                        if (events.length) {
                            // Push into list
                            var arr = [];
                            events.forEach(function(v) {
                                if (v[data.groupBy]){
                                    arr.push(v[data.groupBy])
                                }
                            });
                            // Create the trending list
                            var trending = arrByCount(arr),
                                unit = (data.unit != undefined) ? data.unit : '';
                                _tpl = '<li class="aew-list-header"><small class="_muted">' + unit + '</small></li>';

                            var l = trending.list.slice(0,10);
                            l.forEach(function(v) {
                                _tpl += '<li><span class="_ellipsify">' + v + '</span> <small class="_muted">' + trending.detail[v] + '</small></li>';
                            });

                            tpl += template.trending(_tpl);
                        } else {
                            tpl += template.empty(title);
                        }

                        $(data.container).html(template.render(tpl, 'aew-list'));
                    }
                    $.ajax(ajax);
                },
                feeds: function(data) {
                    var ajax = new globalAjaxSettings();
                    ajax.url = auth.url + '/api/query/raw_data/' + auth.project_id + '/';
                    ajax.data = JSON.stringify(data.data);
                    ajax.success = function(res) {
                        // console.log(res)
                        var events = processApiEvents(res);
                        var title = (data.title != undefined) ? data.title : '';
                        var tpl = '<h3>' + title + '</h3>';
                        if (events.length) {
                            var _tpl = '';
                            res.events.forEach(function(v, k) {
                                var str = '';
                                var parsed = JSON.parse(v.data)
                                _tpl += '<li><span class="_ellipsify-large">';
                                if (data.sentence != undefined) {
                                    data.sentence.forEach(function(_v) {
                                        // console.log(parsed[_v])
                                        if (parsed[_v] != undefined) {
                                            _tpl += parsed[_v] + ' '
                                        }
                                    })
                                }
                                _tpl += '</span> <small class="feeds-muted">' + moment(res.events[k].created).fromNow() + '</small></li>';

                            })
                            tpl += template.trending(_tpl);
                        } else {
                            tpl += template.empty(title);
                        }
                        $(data.container).html(template.render(tpl, ''))
                    }
                    $.ajax(ajax)
                }
            }
        }

// HELPERS ---
        var processApiEvents = function(jsonstr) {
            var raw = [];
            jsonstr.events.forEach(function(v) {
                raw.push(JSON.parse(v.data))
            });
            return raw;
        };

        var arrByCount = function(arr){
            var itm, a = [], L = arr.length, o = {};
            for(var i = 0; i < L; i++){
                itm = arr[i];

                if(!itm) {
                    continue;
                };

                if(o[itm] == undefined) {
                    o[itm] = 1;
                } else {
                    ++o[itm];
                }
            }

            for(var p in o) {
                a[a.length] = p;
            }

            var result = a.sort(function(a, b) {
                return o[b]-o[a];
            });

            return {
                list: result,
                detail: o
            }
        };

        return methods;
    }
};

var chart_helpers = {
    // https://github.com/bebraw/Chart.js.legend/blob/master/src/legend.js
    legend: function(parent, data) {
        legend(parent, data, null);
    },
    legend: function(parent, data, chart, legendTemplate) {
        legendTemplate = typeof legendTemplate !== 'undefined' ? legendTemplate : "<%=label%>";
        parent.className = 'legend';
        var datas = data.hasOwnProperty('datasets') ? data.datasets : data;
        // remove possible children of the parent
        while(parent.hasChildNodes()) {
            parent.removeChild(parent.lastChild);
        }

        var show = chart ? showTooltip : this.noop;
        datas.forEach(function(d, i) {
            // console.log(d);
            //span to div: legend appears to all element (color-sample and text-node)
            var title = document.createElement('div');
            title.className = 'title';
            parent.appendChild(title);

            var colorSample = document.createElement('div');
            colorSample.className = 'color-sample';
            colorSample.style.backgroundColor = d.hasOwnProperty('strokeColor') ? d.strokeColor : d.color;
            colorSample.style.borderColor = d.hasOwnProperty('fillColor') ? d.fillColor : d.color;
            title.appendChild(colorSample);
            legendNode=legendTemplate.replace("<%=value%>",d.value);
            legendNode=legendNode.replace("<%=label%>",d.label);
            var text = document.createTextNode(legendNode);
            text.className = 'text-node';
            title.appendChild(text);

            show(chart, title, i);
        });
    },
    showTooltip: function(chart, elem, indexChartSegment){
        var helpers = Chart.helpers;

        var segments = chart.segments;
        //Only chart with segments
        if(typeof segments != 'undefined'){
            helpers.addEvent(elem, 'mouseover', function(){
                var segment = segments[indexChartSegment];
                segment.save();
                segment.fillColor = segment.highlightColor;
                chart.showTooltip([segment]);
                segment.restore();
            });

            helpers.addEvent(elem, 'mouseout', function(){
                chart.draw();
            });
        }
    },
    noop: function() {}
}